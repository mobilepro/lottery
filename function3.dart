void func3(int a, {int b: 12}) {
  print(" Value of a is $a ");
  print(" Value of b is $b ");
}

void main() {
  // Calling function with default valued parameter
  print(" \n Calling function with default valued parameters ");
  func3(2,b:15);
}
