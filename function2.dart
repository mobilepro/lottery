void func2(var a, {var b, var c}) {
  print(" Value of a is $a ");
  print(" Value of b is $b ");
  print(" Value of c is $c ");
}

void main() {
  // Calling the function with Optional Named parameter
  print(" \n Calling the function with Optional Named parameters : ");
  func2(01,b : 12 ,c: 5);
}
